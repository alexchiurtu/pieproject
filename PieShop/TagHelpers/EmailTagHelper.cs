﻿using Microsoft.AspNetCore.Razor.TagHelpers;

namespace PieShop.TagHelpers
{
    /// <summary>
    /// Email tag helper
    /// </summary>
    public class EmailTagHelper : TagHelper
    {
        /// <summary>
        /// Email Address
        /// </summary>
        public string Address
        {
            get; set;
        }

        /// <summary>
        /// Content of the field
        /// </summary>
        public string Content
        {
            get;
            set;
        }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "a";
            output.Attributes.SetAttribute("href", "mailto:" + Address);
            output.Content.SetContent(Content);
        }
    }
}
