﻿using Microsoft.AspNetCore.Mvc;
using PieShop.Models;
using PieShop.ViewModels;

namespace PieShop.Components
{
    /// <summary>
    /// Shooping cart view
    /// </summary>
    public class ShoppingCartSummary : ViewComponent
    {
        private readonly ShoppingCart _shoppingCart;

        public ShoppingCartSummary(ShoppingCart shoppingCart)
        {
            _shoppingCart = shoppingCart;
        }

        /// <summary>
        /// Returns the Shopping Cart view
        /// The view will contain all the items
        /// that were added in the cart
        /// </summary>
        public IViewComponentResult Invoke()
        {
            var items = _shoppingCart.GetShoppingCartItems;
            _shoppingCart.ShoppingCartItems = items;

            var shoppingCartViewModel = new ShoppingCartViewModel
            {
                ShoppingCart = _shoppingCart,
                ShoppingCartTotal = _shoppingCart.GetShoppingCartTotal
            };
            return View(shoppingCartViewModel);
        }
    }
}
