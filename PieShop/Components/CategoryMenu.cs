﻿using Microsoft.AspNetCore.Mvc;
using PieShop.Models;
using System.Linq;

namespace PieShop.Components
{
    /// <summary>
    /// Represents the category menu
    /// </summary>
    public class CategoryMenu : ViewComponent
    {
        private readonly ICategoryRepository _categoryRepository;
        public CategoryMenu(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
        }

        /// <summary>
        /// Returns a View with all the categories order in an ascendent way
        /// </summary>
        public IViewComponentResult Invoke()
        {
            var categories = _categoryRepository.Categories.OrderBy(c => c.CategoryName);
            return View(categories);
        }
    }
}
