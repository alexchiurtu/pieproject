﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace PieShop.Components
{
    public class AdminMenu : ViewComponent
    {
        /// <summary>
        /// Returns a list of actions that the admin can do
        /// </summary>
        public IViewComponentResult Invoke()
        {
            var menuItems = new List<AdminMenuItem>
            {
                new AdminMenuItem
                {
                    DisplayValue = "User management",
                    ActionValue = "UserManagement"

                },
                new AdminMenuItem
                {
                    DisplayValue = "Role management",
                    ActionValue = "RoleManagement"
                },
                new AdminMenuItem
                {
                    DisplayValue = "Pie management",
                    ActionValue = "PieManagement"
                },
            };

            return View(menuItems);
        }
    }

    /// <summary>
    /// Represent an Admine menu item
    /// </summary>
    public class AdminMenuItem
    {
        /// <summary>
        /// The displayed value of the action
        /// </summary>
        public string DisplayValue { get; set; }
        /// <summary>
        /// The action value 
        /// </summary>
        public string ActionValue { get; set; }
    }
}
