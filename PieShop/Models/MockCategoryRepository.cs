﻿using PieShop.Common;
using System.Collections.Generic;

namespace PieShop.Models
{
    /// <summary>
    /// Mock Class for Categories
    /// </summary>
    public class MockCategoryRepository : ICategoryRepository
    {
        /// <summary>
        /// Mock Categories
        /// </summary>
        public IEnumerable<Category> Categories => Constants.MockCategories;
    }
}
