﻿namespace PieShop.Models
{
    /// <summary>
    /// Interface which describes the contract for the Order Repository
    /// </summary>
    public interface IOrderRepository
    {
        /// <summary>
        /// Creates an order
        /// </summary>
        void CreateOrder(Order order);
    }
}
