﻿namespace PieShop.Models
{
    public class ShoppingCartItem
    {
        /// <summary>
        /// Cart Item Id
        /// </summary>
        public int ShoppingCartItemId
        {
            get;
            set;
        }

        /// <summary>
        /// Pie object
        /// </summary>
        public Pie Pie
        {
            get;
            set;
        }

        /// <summary>
        /// Amount
        /// </summary>
        public int Amount
        {
            get;
            set;
        }

        /// <summary>
        /// Cart Id
        /// </summary>
        public string ShoppingCartId
        {
            get;
            set;
        }
    }
}
