﻿using PieShop.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace PieShop.Models
{
    /// <summary>
    /// Mock Class for Pies
    /// </summary>
    public class MockPieRepository : IPieRepository
    {
        /// <summary>
        /// Mock Pies
        /// </summary>
        public IEnumerable<Pie> Pies => Common.Constants.MockPies;

        /// <summary>
        /// Mock Pies of the week
        /// </summary>
        public IEnumerable<Pie> PiesOfTheWeek => Common.Constants.MockPies.Where(x => x.IsPieOfTheWeek);

        /// <summary>
        /// Mock Pies
        /// </summary>
        public IEnumerable<PieViewModel> PiesAsViewModel => Common.Constants.MockPies
            .Select(x => new PieViewModel
            {
                Name = x.Name,
                PieId = x.PieId,
                Price = x.Price,
                ImageThumbnailUrl = x.ImageThumbnailUrl,
                ShortDescription = x.ShortDescription
            });

        /// <summary>
        /// Mock Pies
        /// </summary>
        public IEnumerable<string> PiesNames => Common.Constants.MockPies.Select(x => x.Name);

        /// <summary>
        /// Add a pie
        /// </summary>
        public bool AddPie(Pie pie)
        {
            Common.Constants.MockPies.Add(pie);
            return true;
        }

        /// <summary>
        /// Delete a pie
        /// </summary>
        public bool DeletePie(Pie pie)
        {
            Common.Constants.MockPies.Remove(pie);
            return true;
        }

        /// <summary>
        /// Get a pie by id
        /// </summary>
        public Pie GetPieById(int pieId) => Common.Constants.MockPies.FirstOrDefault(x => x.PieId == pieId);

        /// <summary>
        /// Update a pie
        /// </summary>
        public bool UpdatePie(Pie pie)
        {
            var pieToUpdate = GetPieById(pie.PieId);
            pieToUpdate.Name = pie.Name;
            pieToUpdate.Price = pie.Price;
            pieToUpdate.IsPieOfTheWeek = pie.IsPieOfTheWeek;
            pieToUpdate.InStock = pie.InStock;
            pieToUpdate.ImageThumbnailUrl = pie.ImageThumbnailUrl;
            pieToUpdate.LongDescription = pie.LongDescription;
            pieToUpdate.ShortDescription = pie.ShortDescription;

            return true;
        }
    }
}
