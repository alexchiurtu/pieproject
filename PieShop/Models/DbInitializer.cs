﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using PieShop.Common;
using System.Linq;

namespace PieShop.Models
{

    /// <summary>
    /// Class used first time when running the app
    /// It seeds the database
    /// </summary>
    public static class DbInitializer
    {

        /// <summary>
        /// Seeds the database for the first time
        /// </summary>
        public static void Seed(IApplicationBuilder applicationBuilder)
        {
            AppDbContext context = applicationBuilder.ApplicationServices.CreateScope().ServiceProvider.GetRequiredService<AppDbContext>();

            if (!context.Categories.Any())
            {
                context.Categories.AddRange(Constants.SeedCategories.Select(c => c.Value));
            }

            if (!context.Pies.Any())
            {
                context.AddRange
                (
                    Constants.SeedPies
                );
            }

            context.SaveChanges();
        }
    }
}
