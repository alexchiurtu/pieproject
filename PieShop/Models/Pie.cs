﻿namespace PieShop.Models
{
    public class Pie
    {
        /// <summary>
        /// ID
        /// </summary>
        public int PieId
        {
            get;
            set;
        }

        /// <summary>
        /// Name
        /// </summary>
        public string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Short Description
        /// </summary>
        public string ShortDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Long Description
        /// </summary>
        public string LongDescription
        {
            get;
            set;
        }

        /// <summary>
        /// Allergy Information
        /// </summary>
        public string AllergyInformation
        {
            get;
            set;
        }

        /// <summary>
        /// Price
        /// </summary>
        public decimal Price
        {
            get;
            set;
        }

        /// <summary>
        /// Image Url
        /// </summary>
        public string ImageUrl
        {
            get;
            set;
        }

        /// <summary>
        /// Image Thumbnail Url
        /// </summary>
        public string ImageThumbnailUrl
        {
            get;
            set;
        }

        /// <summary>
        /// Pie of The Week
        /// </summary>
        public bool IsPieOfTheWeek
        {
            get;
            set;
        }


        /// <summary>
        /// In Stock
        /// </summary>
        public bool InStock
        {
            get;
            set;
        }

        /// <summary>
        /// Category Id
        /// </summary>
        public int CategoryId
        {
            get;
            set;
        }

        /// <summary>
        /// Category Object
        /// </summary>
        public virtual Category Category
        {
            get;
            set;
        }
    }
}
