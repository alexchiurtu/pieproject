﻿using PieShop.ViewModels;
using System.Collections.Generic;

namespace PieShop.Models
{
    /// <summary>
    /// Interface which describes the contract for the Pie Repository
    /// </summary>
    public interface IPieRepository
    {
        /// <summary>
        /// Get Pies
        /// </summary>
        IEnumerable<Pie> Pies
        {
            get;
        }

        /// <summary>
        /// Get Pies of the week
        /// </summary>
        IEnumerable<Pie> PiesOfTheWeek
        {
            get;
        }

        /// <summary>
        /// Returns the Pies as view model
        /// </summary>
        IEnumerable<PieViewModel> PiesAsViewModel
        {
            get;
        }

        /// <summary>
        /// Returns the Pies names
        /// </summary>
        IEnumerable<string> PiesNames
        {
            get;
        }

        /// <summary>
        /// Returns the pies by id
        /// </summary>
        Pie GetPieById(int pieId);

        /// <summary>
        /// Add a new Pie
        /// </summary>
        bool AddPie(Pie pie);

        /// <summary>
        /// Delete a Pie
        /// </summary>
        bool DeletePie(Pie pie);

        /// <summary>
        /// Update a Pie
        /// </summary>
        bool UpdatePie(Pie pie);
    }
}
