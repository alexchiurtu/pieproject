﻿namespace PieShop.Models
{
    public class OrderDetail
    {
        /// <summary>
        /// ID
        /// </summary>
        public int OrderDetailId { get; set; }

        /// <summary>
        /// Order ID
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Pie ID
        /// </summary>
        public int PieId { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Pie object
        /// </summary>
        public virtual Pie Pie { get; set; }

        /// <summary>
        /// Order object
        /// </summary>
        public virtual Order Order { get; set; }
    }
}
