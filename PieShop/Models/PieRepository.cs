﻿using Microsoft.EntityFrameworkCore;
using PieShop.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace PieShop.Models
{
    /// <summary>
    /// Implementaion of the Pie Repository
    /// </summary>
    public class PieRepository : IPieRepository
    {
        private readonly AppDbContext _appDbContext;

        public PieRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        /// <summary>
        /// Gets all the Pies
        /// </summary>
        public IEnumerable<Pie> Pies
        {
            get
            {
                return _appDbContext.Pies.Include(c => c.Category);
            }
        }

        /// <summary>
        /// Gets all the Pies of the week
        /// </summary>
        public IEnumerable<Pie> PiesOfTheWeek
        {
            get
            {
                return _appDbContext.Pies.Include(c => c.Category).Where(p => p.IsPieOfTheWeek);
            }
        }

        /// <summary>
        /// Gets all the Pies names
        /// </summary>
        public IEnumerable<string> PiesNames
        {
            get
            {
                return _appDbContext.Pies.Select(x => x.Name);
            }
        }

        /// <summary>
        /// Gets Pies by Id
        /// </summary>
        public Pie GetPieById(int pieId)
        {
            return _appDbContext.Pies.FirstOrDefault(p => p.PieId == pieId);
        }

        /// <summary>
        /// Gets Pies for Pies view
        /// </summary>
        public IEnumerable<PieViewModel> PiesAsViewModel
        {
            get
            {
                List<PieViewModel> pies = new List<PieViewModel>();

                foreach (var dbPie in Pies)
                {
                    pies.Add(MapDbPieToPieViewModel(dbPie));
                }

                return pies;
            }
        }

        /// <summary>
        /// Add a new Pie
        /// </summary>
        public bool AddPie(Pie pie)
        {
            var result = _appDbContext.Add(pie);

            _appDbContext.SaveChanges();

            return result.State == EntityState.Added || result.State == EntityState.Unchanged;
        }

        /// <summary>
        /// Delete a Pie
        /// </summary>
        public bool DeletePie(Pie pie)
        {
            var result = _appDbContext.Remove(pie);

            _appDbContext.SaveChanges();

            return result.State == EntityState.Deleted || result.State == EntityState.Unchanged;
        }


        /// <summary>
        /// Map Db model to view model
        /// </summary>
        private PieViewModel MapDbPieToPieViewModel(Pie dbPie)
        {
            return new PieViewModel
            {
                PieId = dbPie.PieId,
                Name = dbPie.Name,
                Price = dbPie.Price,
                ShortDescription = dbPie.ShortDescription,
                ImageThumbnailUrl = dbPie.ImageThumbnailUrl
            };
        }


        /// <summary>
        /// Update a pie
        /// </summary>
        public bool UpdatePie(Pie pie)
        {
            _appDbContext.Update(pie);

            _appDbContext.SaveChanges();

            return true;
        }
    }
}
