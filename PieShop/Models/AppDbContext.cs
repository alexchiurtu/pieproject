﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using PieShop.Auth;

namespace PieShop.Models
{
    /// <summary>
    /// Contains all the tables for the EF
    /// </summary>
    public class AppDbContext : IdentityDbContext<ApplicationUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Categories table
        /// </summary>
        public DbSet<Category> Categories
        {
            get;
            set;
        }

        /// <summary>
        /// Pies table
        /// </summary>
        public DbSet<Pie> Pies
        {
            get;
            set;
        }

        /// <summary>
        /// ShoppingCartItems table
        /// </summary>
        public DbSet<ShoppingCartItem> ShoppingCartItems
        {
            get;
            set;
        }

        /// <summary>
        /// Orders table
        /// </summary>
        public DbSet<Order> Orders
        {
            get;
            set;
        }

        /// <summary>
        /// OrderDetails table
        /// </summary>
        public DbSet<OrderDetail> OrderDetails
        {
            get;
            set;
        }
    }
}
