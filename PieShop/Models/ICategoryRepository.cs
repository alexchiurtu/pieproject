﻿using System.Collections.Generic;

namespace PieShop.Models
{
    /// <summary>
    /// Interface which describes the contract for the Category Repository
    /// </summary>
    public interface ICategoryRepository
    {
        /// <summary>
        /// Gets all the categories
        /// </summary>
        IEnumerable<Category> Categories
        {
            get;
        }
    }
}
