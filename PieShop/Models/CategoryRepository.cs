﻿using System.Collections.Generic;

namespace PieShop.Models
{

    /// <summary>
    /// Implementaion of the Category Repository
    /// </summary>
    public class CategoryRepository : ICategoryRepository
    {
        private readonly AppDbContext _appDbContext;

        public CategoryRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }


        /// <summary>
        /// Returns the all the categories
        /// </summary>
        public IEnumerable<Category> Categories => _appDbContext.Categories;
    }
}
