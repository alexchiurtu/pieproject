﻿using System.Collections.Generic;

namespace PieShop.Models
{
    public class Category
    {
        /// <summary>
        /// Category Id
        /// </summary>
        public int CategoryId
        {
            get;
            set;
        }

        /// <summary>
        /// Name of the category
        /// </summary>
        public string CategoryName
        {
            get;
            set;
        }

        /// <summary>
        /// Description of the category
        /// </summary>
        public string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Pies for the category
        /// </summary>
        public List<Pie> Pies
        {
            get;
            set;
        }
    }
}
