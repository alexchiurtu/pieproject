﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using PieShop.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PieShop.Models
{

    public class Order
    {
        /// <summary>
        /// Order ID
        /// </summary>
        [BindNever]
        public int OrderId { get; set; }

        /// <summary>
        /// Orders
        /// </summary>
        public List<OrderDetail> OrderLines { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        [Required(ErrorMessage = "Please enter your first name")]
        [Display(Name = "First name")]
        [StringLength(Constants.FirstNameLength)]
        public string FirstName { get; set; }

        /// <summary>
        /// Last name
        /// </summary>
        [Required(ErrorMessage = "Please enter your last name")]
        [Display(Name = "Last name")]
        [StringLength(Constants.LastNameLength)]
        public string LastName { get; set; }

        /// <summary>
        /// Address line 1
        /// </summary>
        [Required(ErrorMessage = "Please enter your address")]
        [StringLength(Constants.AddressLineLength)]
        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Address line 2
        /// </summary>
        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Zip code
        /// </summary>
        [Required(ErrorMessage = "Please enter your zip code")]
        [Display(Name = "Zip code")]
        [StringLength(Constants.ZipCodeMaxLength, MinimumLength = Constants.ZipCodeMinLength)]
        public string ZipCode { get; set; }

        /// <summary>
        /// City code
        /// </summary>
        [Required(ErrorMessage = "Please enter your city")]
        [StringLength(Constants.CityLength)]
        public string City { get; set; }

        /// <summary>
        /// State code
        /// </summary>
        [StringLength(Constants.StateLength)]
        public string State { get; set; }

        /// <summary>
        /// Country code
        /// </summary>
        [Required(ErrorMessage = "Please enter your country")]
        [StringLength(Constants.CountryLength)]
        public string Country { get; set; }

        /// <summary>
        /// Phone number
        /// </summary>
        [Required(ErrorMessage = "Please enter your phone number")]
        [StringLength(Constants.PhoneNumberLength)]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Email 
        /// </summary>
        [Required]
        [StringLength(Constants.EmailLength)]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(Constants.EmailRegex,
            ErrorMessage = "The email address is not entered in a correct format")]
        public string Email { get; set; }

        /// <summary>
        /// Order total
        /// </summary>
        [BindNever]
        [ScaffoldColumn(false)]
        public decimal OrderTotal { get; set; }

        /// <summary>
        /// OrderPlaced
        /// </summary>
        [BindNever]
        [ScaffoldColumn(false)]
        public DateTime OrderPlaced { get; set; }

        /// <summary>
        /// Pay method
        /// </summary>
        [Required]
        [StringLength(Constants.PayMethodLength)]
        public string PayMethod { get; set; }

        /// <summary>
        /// Card number
        /// </summary>
        [StringLength(Constants.CardNumberLength)]
        public string CardNumber { get; set; }
    }
}
