﻿using System.Collections.Generic;

namespace PieShop.Auth
{
    /// <summary>
    /// This class stores the Claims used for authentication
    /// </summary>
    public static class PieShopClaimTypes
    {
        /// <summary>
        /// List of required claims
        /// </summary>
        public static List<string> ClaimsList { get; set; } = new List<string> { "Delete Pie", "Add Pie", "Age for ordering" };
    }
}
