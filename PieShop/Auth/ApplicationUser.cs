﻿using Microsoft.AspNetCore.Identity;
using System;

namespace PieShop.Auth
{
    /// <summary>
    ///  This class inherits from IndentityUser
    ///  It extends the base fiels of an IdentityUser
    ///  Used to store user data
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        ///  User's birthday
        /// </summary>
        public DateTime Birthdate { get; set; }
        /// <summary>
        ///  User's City
        /// </summary>
        public string City { get; set; }
        /// <summary>
        ///  User's country
        /// </summary>
        public string Country { get; set; }
    }
}