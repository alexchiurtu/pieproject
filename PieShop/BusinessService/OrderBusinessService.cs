﻿using PieShop.Models;
using System.Collections.Generic;

namespace PieShop.BusinessService
{
    public class OrderBusinessService : IOrderBusinessService
    {
        private readonly IOrderRepository _orderRepository;

        public OrderBusinessService(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }

        public void OrderCheckout(Order order, List<ShoppingCartItem> shoppingCartItems)
        {
            foreach (ShoppingCartItem item in shoppingCartItems)
            {
                order.OrderTotal += item.Amount * item.Pie.Price;
            }

            _orderRepository.CreateOrder(order);
        }
    }
}
