﻿using PieShop.Models;
using PieShop.ViewModels;
using System.Collections.Generic;

namespace PieShop.BusinessService
{
    public class ShoppingCartBusinessService : IShoppingCartBusinessService
    {
        private readonly ShoppingCart _shoppingCart;

        public ShoppingCartBusinessService(ShoppingCart shoppingCart)
        {
            _shoppingCart = shoppingCart;
        }

        public void AddToCart(Pie selectedPie)
        {
            _shoppingCart.AddToCart(selectedPie, 1);
        }

        public void ClearCart()
        {
            _shoppingCart.ClearCart();
        }

        public object GenerateViewModel()
        {
            return new ShoppingCartViewModel
            {
                ShoppingCart = _shoppingCart,
                ShoppingCartTotal = _shoppingCart.GetShoppingCartTotal
            };
        }

        public List<ShoppingCartItem> GetShoppingCartItems()
        {
            return _shoppingCart.GetShoppingCartItems;
        }

        public bool IsEmpty()
        {
            return _shoppingCart.ShoppingCartItems.Count == 0;
        }

        public void PrepareShoppingCart()
        {
            _shoppingCart.ShoppingCartItems = _shoppingCart.GetShoppingCartItems;
        }

        public void RemoveFromCart(Pie selectedPie)
        {
            _shoppingCart.RemoveFromCart(selectedPie);
        }
    }
}
