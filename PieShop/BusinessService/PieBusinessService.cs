﻿using PieShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PieShop.BusinessService
{
    public class PieBusinessService : IPieBusinessService
    {
        private readonly IPieRepository _pieRepository;
        private readonly ICategoryRepository _categoryRepository;

        public PieBusinessService(IPieRepository pieRepository, ICategoryRepository categoryRepository)
        {
            _pieRepository = pieRepository;
            _categoryRepository = categoryRepository;
        }

        public Pie GetPieById(int id)
        {
            return _pieRepository.GetPieById(id);
        }

        public IEnumerable<string> GetPiesNames()
        {
            return _pieRepository.PiesNames;
        }

        /// <summary>
        /// Filter the pies by categories
        /// </summary>
        public void FilterByCategory(string category, decimal lowPrice, decimal highPrice, out IEnumerable<Pie> pies, out string currentCategory)
        {
            if (string.IsNullOrEmpty(category))
            {
                pies = _pieRepository.Pies.OrderBy(p => p.PieId);

                pies = PriceFilter(lowPrice, highPrice, pies);

                currentCategory = "All pies";
            }
            else
            {
                pies = _pieRepository.Pies.Where(p => p.Category.CategoryName == category)
                   .OrderBy(p => p.PieId);

                pies = PriceFilter(lowPrice, highPrice, pies);

                currentCategory = _categoryRepository.Categories.FirstOrDefault(c => c.CategoryName == category).CategoryName;
            }
        }

        /// <summary>
        /// Filter the Pies by price
        /// </summary>
        public IEnumerable<Pie> PriceFilter(decimal lowPrice, decimal highPrice, IEnumerable<Pie> pies)
        {
            if (lowPrice != 0)
            {
                return highPrice != 0
                    ? pies.Where(x => x.Price >= lowPrice && x.Price <= highPrice).OrderBy(p => p.PieId)
                    : pies.Where(x => x.Price >= lowPrice).OrderBy(p => p.PieId);
            }
            else if (highPrice != 0)
            {
                return pies.Where(x => x.Price <= highPrice).OrderBy(p => p.PieId);
            }
            else
            {
                return pies;
            }
        }

        /// <summary>
        /// Sort pies 
        /// </summary>
        public IEnumerable<Pie> SortPies(string sorting, IEnumerable<Pie> pies)
        {
            if (!string.IsNullOrEmpty(sorting))
            {
                if (sorting.Equals("ascending", StringComparison.OrdinalIgnoreCase))
                {
                    pies = pies.OrderBy(p => p.Price);
                }
                else if (sorting.Equals("descending", StringComparison.OrdinalIgnoreCase))
                {
                    pies = pies.OrderByDescending(p => p.Price);
                }
                else
                {
                    throw new InvalidOperationException();
                }
            }

            return pies;
        }

        public IEnumerable<Pie> Search(string pieName)
        {
            return _pieRepository.Pies.Where(p => p.Name.Contains(pieName) ||
                p.LongDescription.Contains(pieName) ||
                p.ShortDescription.Contains(pieName)).OrderBy(p => p.PieId);
        }

        public IEnumerable<string> AutoComplete(string term)
        {
            return _pieRepository.Pies.Where(p => p.Name.Contains(term)).Select(p => p.Name).ToList();
        }
    }
}
