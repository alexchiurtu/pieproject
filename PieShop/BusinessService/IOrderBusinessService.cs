﻿using PieShop.Models;
using System.Collections.Generic;

namespace PieShop.BusinessService
{
    public interface IOrderBusinessService
    {
        /// <summary>
        /// Checkout the order
        /// </summary>
        /// <param name="order"></param>
        /// <param name="shoppingCartItems"></param>
        void OrderCheckout(Order order, List<ShoppingCartItem> shoppingCartItems);
    }
}
