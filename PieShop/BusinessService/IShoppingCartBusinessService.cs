﻿using PieShop.Models;
using System.Collections.Generic;

namespace PieShop.BusinessService
{   /// <summary>
    /// Contract for Shopping Cart Business Service
    /// </summary>
    public interface IShoppingCartBusinessService
    {
        void PrepareShoppingCart();
        bool IsEmpty();
        List<ShoppingCartItem> GetShoppingCartItems();
        void ClearCart();
        void RemoveFromCart(Pie selectedPie);
        void AddToCart(Pie selectedPie);
        object GenerateViewModel();
    }
}
