﻿using PieShop.Models;
using System.Collections.Generic;

namespace PieShop.BusinessService
{   /// <summary>
    /// Contract for Pie Bussiness Service
    /// </summary>
    public interface IPieBusinessService
    {
        IEnumerable<string> AutoComplete(string term);
        void FilterByCategory(string category, decimal lowPrice, decimal highPrice, out IEnumerable<Pie> pies, out string currentCategory);
        Pie GetPieById(int id);
        IEnumerable<string> GetPiesNames();
        IEnumerable<Pie> PriceFilter(decimal lowPrice, decimal highPrice, IEnumerable<Pie> pies);
        IEnumerable<Pie> Search(string pieName);
        IEnumerable<Pie> SortPies(string sorting, IEnumerable<Pie> pies);
    }
}