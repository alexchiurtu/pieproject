﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PieShop.ViewModels
{
    public class EditUserViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// User name
        /// </summary>
        [Required(ErrorMessage = "Please enter the user name")]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Required(ErrorMessage = "Please enter the user email")]
        public string Email { get; set; }

        /// <summary>
        /// User claims
        /// </summary>
        public List<string> UserClaims { get; set; }

        /// <summary>
        /// Birthdate
        /// </summary>
        [Required(ErrorMessage = "Please enter the birth date")]
        [Display(Name = "Birth date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }
    }
}