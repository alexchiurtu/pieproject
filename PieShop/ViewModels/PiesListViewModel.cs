﻿using PieShop.Models;
using System.Collections.Generic;

namespace PieShop.ViewModels
{
    public class PiesListViewModel
    {
        /// <summary>
        /// Pies
        /// </summary>
        public IEnumerable<Pie> Pies
        {
            get;
            set;
        }

        /// <summary>
        /// Current Category
        /// </summary>
        public string CurrentCategory
        {
            get;
            set;
        }
    }
}
