﻿using System.ComponentModel.DataAnnotations;

namespace PieShop.ViewModels
{
    public class AddRoleViewModel
    {
        /// <summary>
        /// RoleName
        /// </summary>
        [Required]
        [Display(Name = "Role name")]
        public string RoleName { get; set; }
    }
}