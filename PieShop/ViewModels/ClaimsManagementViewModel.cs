﻿using System.Collections.Generic;

namespace PieShop.ViewModels
{
    public class ClaimsManagementViewModel
    {
        /// <summary>
        /// User Id
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Claim Id
        /// </summary>
        public string ClaimId { get; set; }

        /// <summary>
        /// Claims
        /// </summary>
        public List<string> AllClaimsList { get; set; }
    }
}
