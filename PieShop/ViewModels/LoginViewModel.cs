﻿using System.ComponentModel.DataAnnotations;

namespace PieShop.ViewModels
{
    public class LoginViewModel
    {
        /// <summary>
        /// User Name
        /// </summary>
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        /// <summary>
        /// Password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        /// <summary>
        /// Return Url
        /// </summary>
        public string ReturnUrl { get; set; }
    }
}
