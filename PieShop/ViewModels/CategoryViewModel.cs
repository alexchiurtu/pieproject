﻿namespace PieShop.ViewModels
{
    public class CategoryViewModel
    {
        /// <summary>
        /// ID
        /// </summary>
        public int CategoryId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public int CategoryName { get; set; }
    }
}
