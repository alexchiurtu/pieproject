﻿namespace PieShop.ViewModels
{
    public class AddPieViewModel
    {
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// ShortDescription
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// LongDescription
        /// </summary>
        public string LongDescription { get; set; }

        /// <summary>
        /// AllergyInformation
        /// </summary>
        public string AllergyInformation { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        public decimal Price { get; set; }


        /// <summary>
        /// ImageUrl
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// ImageThumbnailUrl
        /// </summary>
        public string ImageThumbnailUrl { get; set; }

        /// <summary>
        /// IsPieOfTheWeek
        /// </summary>
        public bool IsPieOfTheWeek { get; set; }

        /// <summary>
        /// InStock
        /// </summary>
        public bool InStock { get; set; }

        /// <summary>
        /// Category ID
        /// </summary>
        public int CategoryId { get; set; }
    }
}
