﻿using PieShop.Models;

namespace PieShop.ViewModels
{
    public class ShoppingCartViewModel
    {
        /// <summary>
        /// ShoppingCart
        /// </summary>
        public ShoppingCart ShoppingCart
        {
            get;
            set;
        }

        /// <summary>
        /// ShoppingCartTotal
        /// </summary>
        public decimal ShoppingCartTotal
        {
            get;
            set;
        }
    }
}
