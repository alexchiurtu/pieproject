﻿using PieShop.Models;
using System.Collections.Generic;

namespace PieShop.ViewModels
{
    public class HomeViewModel
    {
        /// <summary>
        /// Pies Of The Week
        /// </summary>
        public IEnumerable<Pie> PiesOfTheWeek { get; set; }

        /// <summary>
        /// Pies Name
        /// </summary>
        public IEnumerable<string> PiesName { get; set; }
    }
}
