﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PieShop.ViewModels
{
    public class EditRoleViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Role Name
        /// </summary>
        [Required(ErrorMessage = "Please enter the role name")]
        [Display(Name = "Role name")]
        public string RoleName { get; set; }

        /// <summary>
        /// Users
        /// </summary>
        public List<string> Users { get; set; }

    }
}