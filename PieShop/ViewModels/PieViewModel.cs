﻿namespace PieShop.ViewModels
{
    public class PieViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int PieId { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Short Description
        /// </summary>
        public string ShortDescription { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Image Thumbnail Url
        /// </summary>
        public string ImageThumbnailUrl { get; set; }
    }
}
