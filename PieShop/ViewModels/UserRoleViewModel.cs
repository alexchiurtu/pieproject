﻿using PieShop.Auth;
using System.Collections.Generic;

namespace PieShop.ViewModels
{
    public class UserRoleViewModel
    {
        public UserRoleViewModel()
        {
            Users = new List<ApplicationUser>();
        }

        /// <summary>
        /// UserId
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// RoleId
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// Users
        /// </summary>
        public List<ApplicationUser> Users { get; set; }

    }
}