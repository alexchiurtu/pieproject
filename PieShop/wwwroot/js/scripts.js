﻿

$(document).ready(function () {

    var carNumber = $("#cardNumber");
    carNumber.hide();

    $("#payMethod").on("change", function () {
        if(this.value === "Cash") {
            carNumber.hide();
        }  else {
            carNumber.show();
        }
    });
});