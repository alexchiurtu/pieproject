﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PieShop.Auth;
using PieShop.Models;
using PieShop.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PieShop.Controllers
{
    /// <summary>
    /// Contains all the logic for the Administrator role
    /// </summary>
    [Authorize(Roles = "Administrators")]
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IPieRepository _pieRepository;
        private readonly ICategoryRepository _categoryRepository;

        public AdminController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IPieRepository pieRepository, ICategoryRepository categoryRepository)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _pieRepository = pieRepository;
            _categoryRepository = categoryRepository;
        }

        /// <summary>
        /// Returns the Administrator view
        /// </summary>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Returns the User Management view
        /// </summary>
        public IActionResult UserManagement()
        {
            var users = _userManager.Users;

            return View(users);
        }

        /// <summary>
        /// Returns the Add User view
        /// </summary>
        public IActionResult AddUser()
        {
            return View();
        }

        /// <summary>
        /// Give to Admin the posibility to add a User
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> AddUser(AddUserViewModel addUserViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(addUserViewModel);
            }

            ApplicationUser user = CreateUser(addUserViewModel);

            IdentityResult result = await _userManager.CreateAsync(user, addUserViewModel.Password);

            if (result.Succeeded)
            {
                return RedirectToAction(Common.Constants.UserManagementView, _userManager.Users);
            }

            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return View(addUserViewModel);
        }



        /// <summary>
        /// Give to Admin the posibility to edit a User
        /// </summary>
        public async Task<IActionResult> EditUser(string id)
        {
            var user = await _userManager.FindByIdAsync(id);

            if (user == null)
            {
                return RedirectToAction(Common.Constants.UserManagementView, _userManager.Users);
            }

            var claims = await _userManager.GetClaimsAsync(user);
            var vm = new EditUserViewModel { Id = user.Id, Email = user.Email, UserName = user.UserName, UserClaims = claims.Select(c => c.Value).ToList() };

            return View(vm);
        }

        /// <summary>
        /// Give to Admin the posibility to edit a User
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> EditUser(EditUserViewModel editUserViewModel)
        {
            var user = await _userManager.FindByIdAsync(editUserViewModel.Id);

            if (user != null)
            {
                user.Email = editUserViewModel.Email;
                user.UserName = editUserViewModel.UserName;
                user.Birthdate = editUserViewModel.Birthdate;
                user.City = editUserViewModel.City;
                user.Country = editUserViewModel.Country;

                var result = await _userManager.UpdateAsync(user);

                if (result.Succeeded)
                {
                    return RedirectToAction(Common.Constants.UserManagementView, _userManager.Users);
                }

                ModelState.AddModelError("", "User not updated, something went wrong.");

                return View(editUserViewModel);
            }

            return RedirectToAction(Common.Constants.UserManagementView, _userManager.Users);
        }

        /// <summary>
        /// Give to Admin the posibility to delete a User
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> DeleteUser(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
                if (result.Succeeded)
                {
                    return RedirectToAction(Common.Constants.UserManagementView);
                }
                else
                {
                    ModelState.AddModelError("", "Something went wrong while deleting this user.");
                }
            }
            else
            {
                ModelState.AddModelError("", "This user can't be found");
            }
            return View(Common.Constants.UserManagementView, _userManager.Users);
        }


        /// <summary>
        /// Returns the Role management view
        /// </summary>
        public IActionResult RoleManagement()
        {
            var roles = _roleManager.Roles;
            return View(roles);
        }

        /// <summary>
        /// Returns the Add New Role view
        /// </summary>
        public IActionResult AddNewRole() => View();

        /// <summary>
        /// Adds a new role into app
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> AddNewRole(AddRoleViewModel addRoleViewModel)
        {

            if (!ModelState.IsValid)
            {
                return View(addRoleViewModel);
            }

            var role = new IdentityRole
            {
                Name = addRoleViewModel.RoleName
            };

            IdentityResult result = await _roleManager.CreateAsync(role);

            if (result.Succeeded)
            {
                return RedirectToAction(Common.Constants.RoleManagementView, _roleManager.Roles);
            }

            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
            return View(addRoleViewModel);
        }

        /// <summary>
        /// Modify an existing role
        /// </summary>
        public async Task<IActionResult> EditRole(string id)
        {
            var role = await _roleManager.FindByIdAsync(id);

            if (role == null)
            {
                return RedirectToAction(Common.Constants.RoleManagementView, _roleManager.Roles);
            }

            var editRoleViewModel = new EditRoleViewModel
            {
                Id = role.Id,
                RoleName = role.Name,
                Users = new List<string>()
            };


            foreach (var user in _userManager.Users)
            {
                if (await _userManager.IsInRoleAsync(user, role.Name))
                {
                    editRoleViewModel.Users.Add(user.UserName);
                }
            }

            return View(editRoleViewModel);
        }

        /// <summary>
        /// Modify an existing role
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> EditRole(EditRoleViewModel editRoleViewModel)
        {
            var role = await _roleManager.FindByIdAsync(editRoleViewModel.Id);

            if (role != null)
            {
                role.Name = editRoleViewModel.RoleName;

                var result = await _roleManager.UpdateAsync(role);

                if (result.Succeeded)
                {
                    return RedirectToAction(Common.Constants.RoleManagementView, _roleManager.Roles);
                }

                ModelState.AddModelError("", "Role not updated, something went wrong.");

                return View(editRoleViewModel);
            }

            return RedirectToAction(Common.Constants.RoleManagementView, _roleManager.Roles);
        }

        /// <summary>
        /// Delete an existing role
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> DeleteRole(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);
            if (role != null)
            {
                var result = await _roleManager.DeleteAsync(role);
                if (result.Succeeded)
                {
                    return RedirectToAction(Common.Constants.RoleManagementView, _roleManager.Roles);
                }

                ModelState.AddModelError("", "Something went wrong while deleting this role.");
            }
            else
            {
                ModelState.AddModelError("", "This role can't be found.");
            }
            return View(Common.Constants.RoleManagementView, _roleManager.Roles);
        }

        /// <summary>
        /// Add a user to a role
        /// </summary>

        public async Task<IActionResult> AddUserToRole(string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);

            if (role == null)
            {
                return RedirectToAction(Common.Constants.RoleManagementView, _roleManager.Roles);
            }

            var addUserToRoleViewModel = new UserRoleViewModel { RoleId = role.Id };

            foreach (var user in _userManager.Users)
            {
                if (!await _userManager.IsInRoleAsync(user, role.Name))
                {
                    addUserToRoleViewModel.Users.Add(user);
                }
            }

            return View(addUserToRoleViewModel);
        }

        /// <summary>
        /// Add a user to a role
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> AddUserToRole(UserRoleViewModel userRoleViewModel)
        {
            var user = await _userManager.FindByIdAsync(userRoleViewModel.UserId);
            var role = await _roleManager.FindByIdAsync(userRoleViewModel.RoleId);

            var result = await _userManager.AddToRoleAsync(user, role.Name);

            if (result.Succeeded)
            {
                return RedirectToAction(Common.Constants.RoleManagementView, _roleManager.Roles);
            }

            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(userRoleViewModel);
        }

        /// <summary>
        /// Delete a user from a role
        /// </summary>
        public async Task<IActionResult> DeleteUserFromRole(string roleId)
        {
            var role = await _roleManager.FindByIdAsync(roleId);

            if (role == null)
            {
                return RedirectToAction(Common.Constants.RoleManagementView, _roleManager.Roles);
            }

            var addUserToRoleViewModel = new UserRoleViewModel { RoleId = role.Id };

            foreach (var user in _userManager.Users)
            {
                if (await _userManager.IsInRoleAsync(user, role.Name))
                {
                    addUserToRoleViewModel.Users.Add(user);
                }
            }

            return View(addUserToRoleViewModel);
        }

        /// <summary>
        /// Delete a user from a role
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> DeleteUserFromRole(UserRoleViewModel userRoleViewModel)
        {
            var user = await _userManager.FindByIdAsync(userRoleViewModel.UserId);
            var role = await _roleManager.FindByIdAsync(userRoleViewModel.RoleId);

            var result = await _userManager.RemoveFromRoleAsync(user, role.Name);

            if (result.Succeeded)
            {
                return RedirectToAction(Common.Constants.RoleManagementView, _roleManager.Roles);
            }

            foreach (IdentityError error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(userRoleViewModel);
        }

        /// <summary>
        /// Returns the pies management view
        /// </summary>
        public IActionResult PieManagement()
        {
            var dbPies = _pieRepository.Pies;

            List<PieViewModel> pies = new List<PieViewModel>();

            foreach (var dbPie in dbPies)
            {
                pies.Add(MapDbPieToPieViewModel(dbPie));
            }

            return View(pies);
        }

        /// <summary>
        /// Returns the add pie view
        /// </summary>
        public IActionResult AddPie()
        {
            ViewData["categories"] = _categoryRepository.Categories.ToList();

            return View();
        }

        /// <summary>
        /// Adds a pie into the app
        /// </summary>
        [HttpPost]
        public IActionResult AddPie(AddPieViewModel addPieViewModel)
        {
            if (!ModelState.IsValid)
            {
                return View(addPieViewModel);
            }

            var pie = new Pie
            {
                Name = addPieViewModel.Name,
                ShortDescription = addPieViewModel.ShortDescription,
                LongDescription = addPieViewModel.LongDescription,
                AllergyInformation = addPieViewModel.AllergyInformation,
                Price = addPieViewModel.Price,
                ImageUrl = addPieViewModel.ImageUrl,
                ImageThumbnailUrl = addPieViewModel.ImageThumbnailUrl,
                IsPieOfTheWeek = addPieViewModel.IsPieOfTheWeek,
                InStock = addPieViewModel.InStock,
                CategoryId = addPieViewModel.CategoryId,

            };

            _pieRepository.AddPie(pie);

            return RedirectToAction(Common.Constants.PieManagementView, _pieRepository.Pies);
        }

        /// <summary>
        /// Delete a pie into the app
        /// </summary>
        [HttpPost]
        public IActionResult DeletePie(int id)
        {
            Pie pie = _pieRepository.GetPieById(id);
            if (pie != null)
            {
                _pieRepository.DeletePie(pie);

                return View(Common.Constants.PieManagementView, _pieRepository.PiesAsViewModel);
            }
            else
            {
                ModelState.AddModelError("", "This pie can't be found.");
            }

            return View(Common.Constants.PieManagementView, _pieRepository.PiesAsViewModel);
        }

        /// <summary>
        /// Edit a pie into the app
        /// </summary>
        public IActionResult EditPie(int id)
        {
            ViewData["categories"] = _categoryRepository.Categories.ToList();

            var pie = _pieRepository.GetPieById(id);

            if (pie == null)
            {
                return View(Common.Constants.PieManagementView, _pieRepository.PiesAsViewModel);
            }

            var editPieViewModel = new EditPieViewModel
            {
                Name = pie.Name,
                ShortDescription = pie.ShortDescription,
                LongDescription = pie.LongDescription,
                AllergyInformation = pie.AllergyInformation,
                Price = pie.Price,
                ImageUrl = pie.ImageUrl,
                ImageThumbnailUrl = pie.ImageThumbnailUrl,
                IsPieOfTheWeek = pie.IsPieOfTheWeek,
                InStock = pie.InStock,
                CategoryId = pie.CategoryId
            };

            return View(editPieViewModel);
        }

        /// <summary>
        /// Edit a pie into the app
        /// </summary>
        [HttpPost]
        public IActionResult EditPie(EditPieViewModel editPieViewModel)
        {
            ViewData["categories"] = _categoryRepository.Categories.ToList();

            var pie = _pieRepository.GetPieById(editPieViewModel.Id);

            if (pie != null)
            {
                pie.Name = editPieViewModel.Name;
                pie.ShortDescription = editPieViewModel.ShortDescription;
                pie.LongDescription = editPieViewModel.LongDescription;
                pie.AllergyInformation = editPieViewModel.AllergyInformation;
                pie.Price = editPieViewModel.Price;
                pie.ImageUrl = editPieViewModel.ImageUrl;
                pie.ImageThumbnailUrl = editPieViewModel.ImageThumbnailUrl;
                pie.IsPieOfTheWeek = editPieViewModel.IsPieOfTheWeek;
                pie.InStock = editPieViewModel.InStock;
                pie.CategoryId = editPieViewModel.CategoryId;

                _pieRepository.UpdatePie(pie);

                return RedirectToAction(Common.Constants.PieManagementView, _pieRepository.PiesAsViewModel);
            }

            return RedirectToAction(Common.Constants.PieManagementView, _pieRepository.PiesAsViewModel);
        }

        /// <summary>
        /// Maps the Db Model to the View Model
        /// </summary>
        private PieViewModel MapDbPieToPieViewModel(Pie dbPie)
        {
            return new PieViewModel
            {
                PieId = dbPie.PieId,
                Name = dbPie.Name,
                Price = dbPie.Price,
                ShortDescription = dbPie.ShortDescription,
                ImageThumbnailUrl = dbPie.ImageThumbnailUrl
            };
        }

        private static ApplicationUser CreateUser(AddUserViewModel addUserViewModel)
        {
            return new ApplicationUser
            {
                UserName = addUserViewModel.UserName,
                Email = addUserViewModel.Email,
                Birthdate = addUserViewModel.Birthdate,
                City = addUserViewModel.City,
                Country = addUserViewModel.Country
            };
        }
    }
}