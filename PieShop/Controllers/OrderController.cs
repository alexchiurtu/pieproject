﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PieShop.BusinessService;
using PieShop.Models;

namespace PieShop.Controllers
{
    /// <summary>
    /// Contains all the methods for the Order page
    /// </summary>
    public class OrderController : Controller
    {
        private readonly IOrderBusinessService _orderBusinessService;
        private readonly IShoppingCartBusinessService _shoppingCartBusinessService;

        public OrderController(IOrderBusinessService orderBusinessService, IShoppingCartBusinessService shoppingCartBusinessService)
        {
            _orderBusinessService = orderBusinessService;
            _shoppingCartBusinessService = shoppingCartBusinessService;
        }

        /// <summary>
        /// Returns the Checkout view
        /// </summary>
        [Authorize]
        public IActionResult Checkout()
        {
            return View();
        }

        /// <summary>
        /// Place the order with the selected items
        /// </summary>
        [HttpPost]
        [Authorize]
        public IActionResult Checkout(Order order)
        {
            _shoppingCartBusinessService.PrepareShoppingCart();
            
            if (_shoppingCartBusinessService.IsEmpty())
            {
                ModelState.AddModelError("", "Your cart is empty, add some pies first");
            }

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model State not valid");
            }

            _orderBusinessService.OrderCheckout(order, _shoppingCartBusinessService.GetShoppingCartItems());

            _shoppingCartBusinessService.ClearCart();

            if (ModelState.IsValid)
            {
                return RedirectToAction("CheckoutComplete");
            }

            return View(order);
        }

        /// <summary>
        /// Returns the Checkout Complete view
        /// </summary>
        public IActionResult CheckoutComplete()
        {
            ViewBag.CheckoutCompleteMessage = "Thanks for your order. You'll soon enjoy our delicious pies.";

            return View();
        }
    }
}