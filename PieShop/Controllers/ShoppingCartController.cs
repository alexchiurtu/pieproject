﻿using Microsoft.AspNetCore.Mvc;
using PieShop.BusinessService;
using PieShop.Models;
using System.Linq;

namespace PieShop.Controllers
{
    /// <summary>
    /// Contains all the methods for ShoppingCart page
    /// </summary>
    public class ShoppingCartController : Controller
    {
        private readonly IPieRepository _pieRepository;
        private readonly IShoppingCartBusinessService _shoppingCartBusinessService;

        public ShoppingCartController(IPieRepository pieRepository, IShoppingCartBusinessService shoppingCartBusinessService)
        {
            _pieRepository = pieRepository;
            _shoppingCartBusinessService = shoppingCartBusinessService;
        }

        /// <summary>
        /// Returns the Shopping Cart page
        /// </summary>
        public IActionResult Index()
        {
            _shoppingCartBusinessService.PrepareShoppingCart();

            var shoppingCartViewModel = _shoppingCartBusinessService.GenerateViewModel();

            return View(shoppingCartViewModel);
        }

        /// <summary>
        /// Add a pie to the shopping cart and returns the ShoppingCart view
        /// </summary>
        public RedirectToActionResult AddToShoppingCart(int pieId)
        {
            var selectedPie = _pieRepository.Pies.FirstOrDefault(p => p.PieId == pieId);

            if (selectedPie != null)
            {
                _shoppingCartBusinessService.AddToCart(selectedPie);
            }
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Remove a pie to the shopping cart and returns the ShoppingCart view
        /// </summary>
        public RedirectToActionResult RemoveFromShoppingCart(int pieId)
        {
            var selectedPie = _pieRepository.Pies.FirstOrDefault(p => p.PieId == pieId);

            if (selectedPie != null)
            {
                _shoppingCartBusinessService.RemoveFromCart(selectedPie);
            }
            return RedirectToAction("Index");
        }
    }
}