﻿using Microsoft.AspNetCore.Mvc;
using PieShop.BusinessService;
using PieShop.Models;
using PieShop.ViewModels;
using System.Collections.Generic;

namespace PieShop.Controllers
{
    /// <summary>
    /// Contains all the methods for the Pie page
    /// </summary>
    public class PieController : Controller
    {
        private readonly IPieBusinessService _pieBusinessService;

        public PieController(IPieBusinessService pieBusinessService)
        {
            _pieBusinessService = pieBusinessService;
        }

        /// <summary>
        /// Returns the pies view list
        /// </summary>
        public ViewResult List(string category, decimal lowPrice = 0, decimal highPrice = 0, string sorting = "")
        {
            string currentCategory = string.Empty;
            _pieBusinessService.FilterByCategory(category, lowPrice, highPrice, out IEnumerable<Pie> pies, out currentCategory);
            pies = _pieBusinessService.SortPies(sorting, pies);

            return View(new PiesListViewModel
            {
                Pies = pies,
                CurrentCategory = currentCategory
            });
        }

        /// <summary>
        /// Search the pies based on the configurations
        /// </summary>
        [HttpGet]
        public ViewResult Search(string pieName, decimal lowPrice = 0, decimal highPrice = 0, string sorting = "")
        {
            IEnumerable<Pie> pies;
            string currentCategory = string.Empty;
            ViewBag.PieName = pieName;
            pies = _pieBusinessService.Search(pieName);
            pies = _pieBusinessService.PriceFilter(lowPrice, highPrice, pies);
            pies = _pieBusinessService.SortPies(sorting, pies);

            return View(new PiesListViewModel
            {
                Pies = pies,
                CurrentCategory = currentCategory
            });
        }



        /// <summary>
        /// Autocomplete for the search bar
        /// </summary>
        [HttpGet]
        public ActionResult AutocompleteSearch()
        {
            try
            {
                string term = HttpContext.Request.Query["term"].ToString();
                var names = _pieBusinessService.AutoComplete(term);

                return Ok(names);
            }
            catch
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Get all the Pies names
        /// </summary>
        [HttpGet]
        public IEnumerable<string> PiesNames()
        {
            return _pieBusinessService.GetPiesNames();
        }

        /// <summary>
        /// Get the Pie details
        /// </summary>
        public IActionResult Details(int id)
        {
            var pie = _pieBusinessService.GetPieById(id);
            if (pie == null)
            {
                return NotFound();
            }

            return View(pie);
        }
    }
}