﻿using Microsoft.AspNetCore.Mvc;
using PieShop.Common;
using PieShop.Models;
using PieShop.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace PieShop.Controllers
{
    /// <summary>
    /// Contains all the methods for Pie Api
    /// </summary>
    [Route("api/[controller]")]
    public class PieDataController : Controller
    {
        private readonly IPieRepository _pieRepository;

        public PieDataController(IPieRepository pieRepository)
        {
            _pieRepository = pieRepository;
        }

        /// <summary>
        /// Loads additional Pies to the website
        /// </summary>
        [HttpGet]
        public IEnumerable<PieViewModel> LoadMorePies()
        {
            IEnumerable<Pie> dbPies = null;

            dbPies = _pieRepository.Pies.OrderBy(p => p.PieId).Take(Constants.NumberOfPiesToLoad);

            List<PieViewModel> pies = new List<PieViewModel>();

            foreach (var dbPie in dbPies)
            {
                pies.Add(MapDbPieToPieViewModel(dbPie));
            }
            return pies;
        }

        /// <summary>
        /// maps the db model to the ViewModel
        /// </summary>
        private PieViewModel MapDbPieToPieViewModel(Pie dbPie)
        {
            return new PieViewModel
            {
                PieId = dbPie.PieId,
                Name = dbPie.Name,
                Price = dbPie.Price,
                ShortDescription = dbPie.ShortDescription,
                ImageThumbnailUrl = dbPie.ImageThumbnailUrl
            };
        }
    }
}
