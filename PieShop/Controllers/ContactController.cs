﻿using Microsoft.AspNetCore.Mvc;

namespace PieShop.Controllers
{
    /// <summary>
    /// Contains all the methods for the Contact page
    /// </summary>
    public class ContactController : Controller
    {
        /// <summary>
        /// Returns Contact page
        /// </summary>
        public IActionResult Index()
        {
            return View();
        }
    }
}