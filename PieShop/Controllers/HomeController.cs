﻿using Microsoft.AspNetCore.Mvc;
using PieShop.Models;
using PieShop.ViewModels;

namespace PieShop.Controllers
{
    /// <summary>
    /// Contains all the methods for the Home page
    /// </summary>
    public class HomeController : Controller
    {
        private readonly IPieRepository _pieRepository;

        public HomeController(IPieRepository pieRepository)
        {
            _pieRepository = pieRepository;
        }

        /// <summary>
        /// Returns the Home view
        /// </summary>
        public ViewResult Index()
        {
            var homeViewModel = new HomeViewModel
            {
                PiesOfTheWeek = _pieRepository.PiesOfTheWeek,
                PiesName = _pieRepository.PiesNames
            };

            return View(homeViewModel);
        }
    }
}
